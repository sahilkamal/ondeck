<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('admin/login');
// });
// Route::get('/dashboard', function () {
//     return view('admin/dashboard');
// });
Route::get('/myloans', function () {
    return view('admin/create_loan');
});
Route::get('/withdraw', function () {
    return view('admin/payment');
});
Route::get('/activity', function () {
    return view('admin/activity');
});
Route::get('/documents', function () {
    return view('admin/documents');
});
Route::get('/update-profile', function () {
    return view('admin/update_profile');
});
Route::get('/choose-bank', function () {
    return view('admin/choose_bank');
});
Route::get('/make-payment', function () {
    return view('admin/make_loan_payment');
});
Route::get('/term-loan-hold', function () {
    return view('admin/term_loan_hold');
});
Route::get('/help', function () {
    return view('admin/help');
});
/*Route::get('/signout', function () {
    return view('admin/signout');
});*/
Route::resource('users', 'UserController');
Auth::routes();
Route::get('/','UserController@login');
Route::get('/check_email/{id}','UserController@CheckEmailExistance');
Route::get('/dashboard','LoanController@getLoanDetails');
Route::post('/show_active_loan','LoanController@showActiveLoan');

