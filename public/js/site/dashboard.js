/*
* Author    : MSN
* Created at: 26-7-2017
* Version   : 1.0
*/

$(document).on('change', '#amount_enter_checkbox', function() {
    if (this.checked) {
        $('.exact_input_field').show();
        $('.loan_dropdown_ds').hide();
    }else{
     $('.loan_dropdown_ds').show();
     $('.exact_input_field').hide();
    }
});
  $(function($){
   $("#entered_amount").mask("99,999.99",{completed:function(){}});
});

function showChat(){
    $( ".chat-window" ).show();
    $('.minimize_icon').addClass('fa fa-minus')

}
// minimize chat box
$(document).on('click', '.minimize_icon', function (e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();

        $this.addClass('panel-collapsed');
        $this.removeClass('fa fa-minus');
        $this.addClass('fa fa-plus');
        //$this.removeClass('fa fa-plus');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.removeClass('fa fa-plus')
        $this.addClass('fa fa-minus');
        //$this.removeClass('fa fa-minus');
    }
});
$(document).on('click', '.icon_close', function (e) {
    //$(this).parent().parent().parent().parent().remove();
    $( "#chat_window" ).hide();

});

// For Input Field //
$(document).on('click', '#other_bank', function() {
    $('.hide_input_sec').show();
     $('.continue_btn').hide();
    $('.input_length').show();
    $('.bank_select_btn').show();
    // End input field//
});

$(document).on('click', '.bank_selector', function() {
    $('.continue_btn').show();
     $('.bank_select_btn').hide();
    $('.input_length').hide();
    // End input field//
});

// function to select dropdownvalue
$("#current_loans").change(function () {
    var selectedLoanText = $('#current_loans option:selected').text();
    var selectedLoanId = $('#current_loans option:selected').val();

    $.ajax({
        type: 'POST',
        url: 'show_active_loan',
        data: {
             loan_id: selectedLoanId,
        },
        dataType: 'json',
        success: function (data) {
            if(data.message == 'success'){
                $('.chosen_loan').text(selectedLoanText);
            }
            
        }
    });
    });

