//progress Bar//
// progressbar.js@1.0.0 version is used
// Docs: http://progressbarjs.readthedocs.org/en/1.0.0/

var bar = new ProgressBar.Circle(progress, {
    color: '#aaa',
    // This has to be the same size as the maximum width to
    // prevent clipping
    strokeWidth: 4,
    trailWidth: 1,
    easing: 'easeInOut',
    duration: 1400,
    text: {
        autoStyleContainer: false
    },
    from: {
        color: '#0074ff',
        width: 0
    },
    to: {
        color: '##0074ff',
        width: 4
    },
    // Set default step function for all animate calls
    step: function (state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);

        var value = Math.round(circle.value() * 100) + '%';
        if (value === 0) {
            circle.setText('');
        } else {
            circle.setText(value);
        }

    }
});
bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
bar.text.style.fontSize = '2rem';

bar.animate(0.71); // Number from 0.0 to 1.0

// show menu button active
$(document).ready(function () {
    $("li a").click(function () {
        $('.nav-bar li').removeClass();
        $(this).parent().addClass('big_caret_icon');
    });
});

// Term Loans page circle script

//progress Bar//
// progressbar.js@1.0.0 version is used
// Docs: http://progressbarjs.readthedocs.org/en/1.0.0/

var bar = new ProgressBar.Circle(term, {
    color: '#aaa',
    // This has to be the same size as the maximum width to
    // prevent clipping
    strokeWidth: 4,
    trailWidth: 1,
    easing: 'easeInOut',
    duration: 1400,
    text: {
        autoStyleContainer: false
    },
    from: {
        color: '#0074ff',
        width: 4
    },
    to: {
        color: '##0074ff',
        width: 20
    },
    // Set default step function for all animate calls
    step: function (state, circle) {
        circle.path.setAttribute('stroke', state.color);
        circle.path.setAttribute('stroke-width', state.width);

        var value = Math.round(circle.value() * 100) + '%' + '<br>' + '<p class="progress_text">paid</p>';
        if (value === 0) {
            circle.setText('');
        } else {
            circle.setText(value);
        }

    }
});
bar.text.style.fontFamily = '"Raleway", Helvetica, sans-serif';
bar.text.style.fontSize = '2rem';

bar.animate(0.25); // Number from 0.0 to 1.0

function editEmail() {

    var edit_button = $('#account_email').text();
    //                alert(edit_button);
    if (edit_button == 'hide') {
        $('#email').addClass('inline');
        $('#account_email').text('(edit)');
        $('#email').removeClass('js_class');
    } else {
        $('#email').addClass('js_class');
        $('#account_email').text('hide');
        $('#email').removeClass('inline');
    }

}

function editPassword() {
    var edit_button = $('#account_pass').text();
    if (edit_button == 'hide') {
        $('#pass').addClass('inline');
        $('#account_pass').text('(edit)');
        $('#pass').removeClass('js_class');
    } else {
        $('#pass').addClass('js_class');
        $('#account_pass').text('hide');
        $('#pass').removeClass('inline');
    }
}
