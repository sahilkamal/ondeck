<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loans extends Model
{
    protected  $table = "loans";
    protected $fillable = [
        'amount', 'status', 'type','principal','interest','loan_period','user_id'
    ];
}
