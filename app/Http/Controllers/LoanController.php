<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Loans;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;

class LoanController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    public function getLoanDetails(){

    	$user_id = Auth::user()->id;
        //return $user_id;
    	$loans = Loans::where('loans_user_id_foreign', $user_id)->get();
    	//return $loans; 
    	return view('admin.dashboard', compact('loans'));
    }

    public function showActiveLoan(Request $request){
    	$loans = Loans::find($request->loan_id);
		$loans->status = 1;
		$loans->save();
		return json_encode(['message'=> 'success']);

    }
}
