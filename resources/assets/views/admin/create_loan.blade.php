@extends('layouts.master')
@section('content')
         <!-- Start Navigation-->
        <section class="navigation">
            <div class="container">
                <div class="card">
                    <ul class="sub_menu nav nav-tabs underline" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#loan_page" aria-controls="loan_page" role="tab" data-toggle="tab">Select Loan Amount</a>
                        </li>
                        <li role="presentation">
                            <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Review Loan Details</a>
                        </li>
                        <li role="presentation">
                            <a href="#transfer_confrm" aria-controls="settings" role="tab" data-toggle="tab">Transfer Confirmation</a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- End Navigation-->

        <!-- start Body Container -->

        <!--crow_1_2-->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="loan_page">
                <div class="body_color">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9 col-sm-8">
                                <div class="adjust_sec">
                                    <div class="amount_sec">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="colr_text">
                                                <h3>Select your loan</h3>
                                            </div>
                                            <div class="pre_heading">
                                                <p>Loan amount</p>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <select>
                                                <option>$5,700</option>
                                                <option>$6,000</option>
                                                <option>$6,500</option>
                                            </select>
                                        </div> 
                                        <div class="col-md-4 col-sm-8">
                                            <div class="term_duration">
                                                <p>Terms Length : <strong>6 Months </strong></p>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="radio_check p-t-10">
                                                <input type="checkbox" name="" id="">
                                                <span class="colr_text">Enter exect amount</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4">
                                <div class="right_sec">
                                    <div class="col-md-12 col-sm-12 p-0">
                                        <div class="balance_info">
                                            <span class="pull-left ligh_text">Account Details</span>
                                        </div>
                                        <div class="main_details no_border">
                                            <div class="further_account_detail">
                                                <span class="pull-left small_text">Available Funds</span>
                                                <span class="pull-right clr_black"> <strong>$5,700.00</strong></span>
                                            </div>
                                            <div class="further_account_detail">
                                                <span class="pull-left small_text">Outstanding Balance</span>
                                                <span class="pull-right clr_black"> <strong>$13,828.75</strong></span>
                                            </div>  
                                            <div class="further_account_detail">
                                                <span class="pull-left small_text">Next Due Date</span>
                                                <span class="pull-right clr_black"><strong>n/a</strong></span>
                                            </div>
                                            <div class="further_account_detail">
                                                <span class="pull-left small_text">Mininum Due</span>
                                                <span class="pull-right clr_black"><strong>n/a</strong></span>
                                            </div>     
                                        </div>
                                    </div>
                                    <div class="payment_link">
                                        <a href="#" class="colr_text"><strong> Deposite Location </strong></a>
                                    </div>
                                    <div class="bank_name">
                                        <img src="images/pnc.gif" alt="No-img"><label><strong>PNC Bank-*6804</strong></label><a href="#"><i class="fa fa-question" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-8 m-t-10">
                                <div class="table_content">
                                    <div class="amount_sec">
                                        <div class="col-md-12 col-sm-12">
                                            <div class="colr_text">
                                                <h3>Review  payment schedule</h3>
                                            </div>
                                            <div class="pre_heading">
                                                <p>This Amount Will be replied over six months through automatic withdrawls. There are no early payment penalities. so you can save money when you pay off the entire loan balance early.</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12">
                                            <!-- Nav tabs -->
                                            <div class="card">
                                                <ul class="nav nav-tabs tabs" role="tablist">
                                                    <li role="presentation" class="active">
                                                        <a href="#home" aria-controls="home" role="tab" data-toggle="tab">All My Loans</a>
                                                    </li>
                                                    <li role="presentation">
                                                        <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">New Loan</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-sm-12">
                                            <div class="loan_offer">
                                                <p class="pull-left">new loan + existing outstanding loans</p>   
                                            </div>
                                        </div>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="home">
                                                <div class="payment_chart">
                                                    <table class="payment_table">
                                                        <tr>
                                                            <th>Date</th>
                                                            <th>Principal</th>
                                                            <th>Fee</th>
                                                            <th>Total Due</th>
                                                        </tr>
                                                        <tr>
                                                            <td>8/10/2017</td>
                                                            <td>$2,575.00</td>
                                                            <td>$1,142.25</td>
                                                            <td><strong>$3,717.25</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9/10/2017</td>
                                                            <td>$2,575.00</td>
                                                            <td>$1,142.25</td>
                                                            <td><strong>$3,717.25</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10/10/2017</td>
                                                            <td>$2,575.00</td>
                                                            <td>$252.00</td>
                                                            <td><strong>$2,827.00</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11/10/2017</td>
                                                            <td>$2,575.00</td>
                                                            <td>$252.00</td>
                                                            <td><strong>$2,827.00</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>12/11/2017</td>
                                                            <td>$2,575.00</td>
                                                            <td>$252.00</td>
                                                            <td><strong>$2,827.00</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>01/10/2018</td>
                                                            <td>$2,575.00</td>
                                                            <td>$252.00</td>
                                                            <td><strong>$2,827.00</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>02/10/2018</td>
                                                            <td>$1,625.00</td>
                                                            <td>$195.00</td>
                                                            <td><strong>$1,820.00</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>03/10/2018</td>
                                                            <td>$1,625.00</td>
                                                            <td>$195.00</td>
                                                            <td><strong>$1,820.00</strong></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="profile">
                                                <div class="payment_chart">
                                                    <table class="payment_table">
                                                        <tr>
                                                            <th>Date</th>
                                                            <th>Principal</th>
                                                            <th>Fee</th>
                                                            <th>Total Due</th>
                                                        </tr>
                                                        <tr>
                                                            <td>8/10/2017</td>
                                                            <td>$2,575.00</td>
                                                            <td>$1,142.25</td>
                                                            <td><strong>$3,717.25</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>9/10/2017</td>
                                                            <td>$2,575.00</td>
                                                            <td>$1,142.25</td>
                                                            <td><strong>$3,717.25</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>10/10/2017</td>
                                                            <td>$2,575.00</td>
                                                            <td>$252.00</td>
                                                            <td><strong>$2,827.00</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>11/10/2017</td>
                                                            <td>$2,575.00</td>
                                                            <td>$252.00</td>
                                                            <td><strong>$2,827.00</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>12/11/2017</td>
                                                            <td>$2,575.00</td>
                                                            <td>$252.00</td>
                                                            <td><strong>$2,827.00</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>01/10/2018</td>
                                                            <td>$2,575.00</td>
                                                            <td>$252.00</td>
                                                            <td><strong>$2,827.00</strong></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 p-0">
                                            <ul class="fee_summary">
                                                <li>All Loans:$18,700.00</li>
                                                <li>Fees:$3,682.450</li>
                                                <li><strong>Loans + Fees: $22,382.50 </strong></li>
                                            </ul>
                                        </div>
                                        <div class="col-md-12 col-sm-12 p-0">
                                            <div class="review_btn pull-right">
                                                <a href="#">Continue to Review
                                                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="payment_info_detail small_text">
                                            <p> future payments show assume the standard repayment schedule is followed. Numbers are not adjusted for pending, late missed or returned paymments or for paying your loan off early.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4">
                                <div class="right_sec">
                                    <ul class="question_sec">
                                        <li class="colr_text">Frequently Asked Questions</li>
                                        <li><a href="#" class="colr_text">How to loan payments works ?</a></li>
                                        <li><a href="#" class="colr_text">How are my costs calculated ?</a></li>
                                        <li><a href="#" class="colr_text">How can i change my deposit location ?</a></li>
                                        <li><a href="#" class="colr_text">Are there prepayment panalties ?</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div role="tabpanel" class="tab-pane" id="settings">
                <div class="body_color">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="adjust_sec">
                                    <div class="amount_sec">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 p-0 funds">
                                                <div class="colr_text">
                                                    <h3>Confirm your loan request?</h3>
                                                </div>                                        
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div class="terms">
                                                    <div class="col-md-12 col-sm-12 p-0 border">
                                                        <i class="fa fa-check-circle-o pull_left" aria-hidden="true"></i>
                                                        <span>You have requested a<strong> $5,700.0 </strong>loan with a <strong>6-month term</strong></span>
                                                        <a href="#" class="pull-right colr_text">Modify</a>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 p-0 border">
                                                        <i class="fa fa-check-circle-o pull_left" aria-hidden="true"></i>
                                                        <span>Your fund will be transferred to: <strong>PNC Bank-*6804</strong></span>
                                                        <a href="#" class="pull-right colr_text">Modify</a>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 p-0 border">
                                                        <i class="fa fa-check-circle-o pull_left" aria-hidden="true"></i>
                                                        <span>The first payment for <a href="#" class="text_underline"> <strong>this loan </strong></a> is <strong>$1,263.50</strong> and is due <strong>August 10, 2017</strong></span>
                                                        <p><a href="#" class="colr_text">View payment schedule</a></p>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 p-0">
                                                    <div class="code_link">
                                                        <a href="#" class="colr_text">Enter promo code</a>
                                                    </div>
                                                </div> 
                                                <div class="col-md6 col-sm-6 p-0">
                                                    <div class="back_btn pull-left">
                                                        <a href="#">Back</a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-6 p-0">
                                                    <div class="fund_btn pull-right">
                                                        <a href="#">Withdraw funds
                                                            <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
                                                        </a>
                                                        <div class="loan_timing">
                                                            <p class="m-t-10 pull-right">Only 1 loan can be take every 24 hours</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="transfer_confrm">
                <div class="body_color">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="adjust_sec">
                                    <div class="amount_sec">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 p-0 funds">
                                                <div class="colr_text">
                                                    <h3>Confirm your loan request?</h3>
                                                </div>                                        
                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <label>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @stop
        