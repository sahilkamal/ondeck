		@extends('layouts.master')
		@section('content')
        <!-- Start Navigation-->
        <section class="navigation">
            <div class="container">
                <ul class="sub_menu">
                    <li><a href="#"><span class="caret_icon">Activity</span></a></li>
                </ul>
            </div>
        </section>
        <!-- End Navigation-->

        <!-- start Body Container -->

        <!--crow_1_2-->
        <div class="body_color">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8">
                        <div class="adjust_sec">
                            <div class="amount_sec">
                                <div class="col-md-12 col-sm-12 p-0">
                                    <div class="colr_text">
                                        <h4>Activity</h4>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 p-0">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <ul class="nav nav-tabs tabs" role="tablist">
                                            <li role="presentation" class="active">
                                                <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Transactions</a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Loan History</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="home">
                                        <div class="payment_chart">
                                            <div class="col-md-4 col-sm-4 p-0">
                                                <div class="form-group">
                                                    <p>From:</p>
                                                    <div class='input-group date' id='datetimepicker1'>
                                                        <input type='text' class="form-control customize" />
                                                        <span class="input-group-addon icon_setting"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4">
                                                <div class="form-group">
                                                    <p>To:</p>
                                                    <div class='input-group date' id='datetimepicker1'>
                                                        <input type='text' class="form-control customize" />
                                                        <span class="input-group-addon icon_setting"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 p-0">
                                                <div class="form-group">
                                                    <p>Type</p>
                                                    <div class="select_input">
                                                        <select class="form-control">
                                                            <option selected="selected">All</option>
                                                            <option >MSB</option>
                                                            <option >ABC</option>
                                                            <option >XYZ</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 p-0">
                                                <table class="active_page_table">
                                                    <tr>
                                                        <th>Date</th>
                                                        <th>Description</th>
                                                        <th>Debit</th>
                                                        <th>Credit</th>
                                                        <th>Balance</th>
                                                    </tr>
                                                    <tr>
                                                        <td>07/11/2017 </td>
                                                        <td>Automatic Draft Remittance-Thank you</td>
                                                        <td></td>
                                                        <td>$2,453.75</td>
                                                        <td>$13,828.75</td>
                                                    </tr>
                                                    <tr>
                                                        <td>07/06/2017</td>
                                                        <td>Loan Charge 5 of 12-Plan#768129</td>
                                                        <td>$828.75</td>
                                                        <td></td>
                                                        <td>16,282.50</td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col -md-12 col-sm-12 p-0">
                                                <div class="payment_link p-t-10">
                                                    <a href="#" class="colr_text pull-right"> View loan history</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="profile">
                                        <div class="payment_chart">
                                            <div class="col-md-12 col-sm-12 p-0">
                                                <table class="payment_table">
                                                    <tr class="dark_border">
                                                        <th>Date</th>
                                                        <th>Original Loan Amount</th>
                                                        <th>Description</th>
                                                        <th>Outstanding Balance</th>
                                                    </tr>
                                                    <tr class="dark_border">
                                                        <td>03/06/2017</td>
                                                        <td>$19,500.00</td>
                                                        <td>Direct Deposit Loan #768129</td>
                                                        <td><strong>$13,828.75</strong></td>
                                                    </tr>
                                                </table>
                                                <div class="transaction_notice">
                                                    <p>This is a historical snapshot of your transaction history as of close of business (midnight) of the previous day.</p>
                                                </div>
                                                <div class="col -md-12 col-sm-12 p-0">
                                                    <div class="payment_link p-t-10">
                                                        <a href="#" class="colr_text pull-right"> View complete transaction history</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="right_sec sec_length">
                            <div class="col-md-12 col-sm-12 p-0">
                                <div class="balance_info solid_border">
                                    <span class="pull-left ligh_text">Account Details</span>
                                </div>
                                <div class="main_details no_border">
                                    <div class="further_account_detail">
                                        <span class="pull-left small_text">Available Funds</span>
                                        <span class="pull-right clr_black"> <strong>$5,700.00</strong></span>
                                    </div>
                                    <div class="further_account_detail">
                                        <span class="pull-left small_text">Outstanding Balance</span>
                                        <span class="pull-right clr_black"> <strong>$13,828.75</strong></span>
                                    </div>  
                                    <div class="further_account_detail">
                                        <span class="pull-left small_text">Amount Past Due</span>
                                        <span class="pull-right clr_black"><strong>$0.00</strong></span>
                                    </div>
                                    <div class="further_account_detail">
                                        <span class="pull-left small_text">Total Credit Line</span>
                                        <span class="pull-right clr_black"><strong>$19,500.00</strong></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <div class="right_sec p-b-30">
                            <ul class="question_sec Heading_border">
                                <li class="colr_text solid_border">Monthly Statements</li>
                            </ul>
                            <p class="m-b-0">Download Statement</p>
                            <div class="col-md-8 col-sm-8 p-0">
                                <div class="statement_option">
                                    <select name="download" id="download">
                                        <option value="selected">06/20/2017</option>
                                        <option value="selected">07/20/2017</option>
                                        <option value="selected">08/20/2017</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 p-0">
                                <div class="download_link pull-right">
                                    <a href="#"><i class="fa fa-download" aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            @stop