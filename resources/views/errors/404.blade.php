@extends('layouts.master')
@section('content')
<div class="term_bg_colr no_border">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 p-0">
                        <div class="col-md-6 col-sm-6 p-0">
                            <div class="large_text">
                                <h1><strong>Oh no!</strong></h1>
                            </div>
                            <div class="error_detail">
                                <h2><strong>We can't seem to find the page you're looking for.</strong></h2>
                                <p>Error code:404</p>
                            </div>
                            <div class="home_link">
                                <a href="dashboard" class="colr_text">Take me back home</a>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 p-0">
                            <div class="sad_emoji">
                                <i class="fa fa-frown-o" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop