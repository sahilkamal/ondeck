<!DOCTYPE html>
<html lang="en" class="login_bg">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Log in Page</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>

    <body>
        <header>
            <!--start pre Header-->
            <div class="pre_header">
                <div class="container">
                    <ul class="pre_header_list pull-right">
                        <li><a href="#">chat live</a></li>
                        <li><strong>Call Us:</strong>1.888 726.2198</li>
                    </ul>
                </div>
            </div>
        </header>
        <!--End pre Header-->

        <!--Star Main Header -->
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-9 p-0">
                    <div class="logo resize">
                        <img src="images/logo.svg" alt="No-Img" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
                <!--End Main Header-->
        <!--Log In page-->
        <div class="top_borders">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 p-0">
                    <div class="col-md-9 col-sm-9 p-0">
                        <div class="sign_out_header m-b-30 m-t-30">
                            <h1>You're sign out</h1>
                            <div class="sign_out_text">
                                <p>You've been securely signed out of your account</p>
                                <span>Thank you for being an OnDeck customer.</span>
                                <ul class="signin_back m-t-20">
                                    <li><a href="/" class="colr_text"><strong>Sign Back in</strong></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <div class="right_side_info m-b-30 m-t-30">
                            <p><strong>Questions?</strong></p>
                            <small>We’re here to help answer your questions or take your application over the phone.</small>
                            <h2 class="m-t-10">(888) 828-5717</h2>
                            <div class="email_chat_link bottom_border p-b-20 m-t-10">
                                <a href="#" class="colr_text"><small>Email Support</small></a>
                                <a href="#" class="colr_text"><small>Click to Chat</small></a>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </body>
</html>