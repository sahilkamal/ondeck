@extends('layouts.master')
@section('content')
<div class="term_bg_colr">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="page_header">
                            <h1>Term Loan</h1>
                        </div>
                        <span class="balance_detail pull-right">$40,000.00 LOAN # 1866745442309553</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 p-0">
                    <div class="main_error_sec">
                        <div class="error_details">
                            <i class="fa fa-exclamation-triangle" aria-hidden="true"></i>
                            <h3>Your Term Loan is on hold due to previously missed payments</h3>
                        </div>
                        <ul class="top_points">
                            <li>We will not process auto-payments for your Term Loan</li>
                            <li>Payment and collection on your other OnDeck products will not be affected</li>
                        </ul>
                        <ul class="bottom_points">
                            <li>Please contact our payment support team if you haven't done so already</li>
                        </ul>
                        <ul class="contact_and_email">
                            <li>
                                <i class="fa fa-phone" aria-hidden="true"></i> 888-994-6603
                            </li>
                            <li>
                                <i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="#" class="colr_text">paymentsupport@ondeck.com</a>
                            </li>
                            <li>
                                <i class="fa fa-clock-o" aria-hidden="true"></i> Mon-Fri 8am - 9pm EST, Sat 8am - 4:30pm EST 
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="top_borders bottom_border">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 p-0">
                        <div class="bottom_description">
                            <h1><strong> This loan is closed. </strong></h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="Term_loan_top_sec">
                        <div class="col-md-4 col-sm-4">
                            <div class="border_right">
                                <div class="col-md-4 col-sm-4">
                                    <div class="total_balance_sec opacity">
                                        <div id="circle"></div>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-8 p-0">
                                    <div class="remaining_amount">
                                        <h2>$0.00</h2>
                                        <p>Outstanding balance of <strong>$53,560.08 Loan</strong></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="border_right p-r-30">
                                <div class="col-md-4 col-sm-4 p-0">
                                    <div class="clander_icon">
                                        <i class="fa fa-calendar" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-8 p-0">
                                    <div class="date_holder">
                                        <h2>Oct 20th, 2017</h2>
                                        <p>Approximate payoff date</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-sm-4 p-0">
                            <div class="">
                                <div class="col-md-12 col-sm-12 p-0">
                                    <div class="clander_icon">
                                        <small>REMAINING PAYMENTS</small>
                                        <h2>0</h2>
                                        <div class="loan_text">
                                            <small>LOAN PERIOD</small>
                                            <h2>12 Months</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 col-sm-8">
                    <!-- Nav tabs -->
                    <div class="card">
                        <ul class="nav nav-tabs tabs" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#home" aria-controls="home" role="tab" data-toggle="tab"> Transaction History</a>
                            </li>
                            <li role="presentation">
                                <a href="#notification" aria-controls="profile" role="tab" data-toggle="tab">Notification</a>
                            </li>
                        </ul>
                    </div>
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="table_main_sec">
                                <!--Table Header-->
                                <div class="col-md-2 col-sm-2">
                                    <div class="table_header">
                                        <p>Date</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="table_header">
                                        <p>Type</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="table_header">
                                        <p>Status</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="table_header">
                                        <p>Amount</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="table_header">
                                        <p>Principal</p>
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="table_header">
                                        <p>Interest</p>
                                    </div>
                                </div>
                                <!--End Table Header-->
                                <!-- Table Content-->
                                <div class="col-md-2 col-sm-2">
                                    <div class="table_item">
                                        07/21/2017
                                    </div>
                                    <div class="table_item">
                                        07/10/2017
                                    </div>
                                    <div class="table_item">
                                        07/19/2017
                                    </div>
                                    <div class="table_item">
                                        07/18/2017
                                    </div>
                                    <div class="table_item">
                                        07/17/2017
                                    </div>
                                    <div class="table_item">
                                        07/14/2017
                                    </div>
                                    <div class="table_item">
                                        07/13/2017
                                    </div>
                                    <div class="table_item">
                                        07/12/2017
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="table_item">
                                        Loan Payment
                                    </div>
                                    <div class="table_item">
                                        Loan Payment
                                    </div>
                                    <div class="table_item">
                                        Loan Payment
                                    </div>
                                    <div class="table_item">
                                        Loan Payment
                                    </div>
                                    <div class="table_item">
                                        Loan Payment
                                    </div>
                                    <div class="table_item">
                                        Loan Payment
                                    </div>
                                    <div class="table_item">
                                        Loan Payment
                                    </div>
                                    <div class="table_item">
                                        Loan Payment
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="table_item">
                                        Pending
                                    </div>
                                    <div class="table_item">
                                        Completed
                                    </div>
                                    <div class="table_item">
                                        Completed
                                    </div>
                                    <div class="table_item">
                                        Completed
                                    </div>
                                    <div class="table_item">
                                        Completed
                                    </div>
                                    <div class="table_item">
                                        Completed
                                    </div>
                                    <div class="table_item">
                                        Completed
                                    </div>
                                    <div class="table_item">
                                        Completed
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="table_item">
                                        $204.60
                                    </div>
                                    <div class="table_item">
                                        $204.60
                                    </div>
                                    <div class="table_item">
                                        $204.60
                                    </div>
                                    <div class="table_item">
                                        $204.60
                                    </div>
                                    <div class="table_item">
                                        $204.60
                                    </div>
                                    <div class="table_item">
                                        $204.60
                                    </div>
                                    <div class="table_item">
                                        $204.60
                                    </div>
                                    <div class="table_item">
                                        $204.60
                                    </div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="table_item">
                                        $134.75
                                    </div>
                                    <div class="table_item">
                                        $134.47
                                    </div>
                                    <div class="table_item">
                                        $134.19
                                    </div>
                                    <div class="table_item">
                                        $133.91
                                    </div>
                                    <div class="table_item">
                                        $133.62
                                    </div>
                                    <div class="table_item">
                                        $133.34
                                    </div>
                                    <div class="table_item">
                                        $133.06
                                    </div>
                                    <div class="table_item">
                                        $132.79
                                    </div>

                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="table_item">
                                        $70.13
                                    </div>
                                    <div class="table_item">
                                        $69.85
                                    </div>
                                    <div class="table_item">
                                        $70.41
                                    </div>
                                    <div class="table_item">
                                        $70.69
                                    </div>
                                    <div class="table_item">
                                        $70.98
                                    </div>
                                    <div class="table_item">
                                        $71.26
                                    </div>
                                    <div class="table_item">
                                        $71.54
                                    </div>
                                    <div class="table_item">
                                        $71.81
                                    </div>
                                </div>
                                <!--End Table Content-->
                            </div>

                            <div class="col-md-12 col-sm-12 p-0">
                                <div class="table_detalis">
                                    <a href="#" class="colr_text">Show More <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                    <a href="#" class="colr_text pull-right"> <i class="fa fa-download" aria-hidden="true"></i> Download CSV</a>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="notification">
                            <div class="table_main_sec">
                               <!-- Table Header-->
                                <div class="col-md-6 col-sm-6 p-0">
                                    <div class="table_header">
                                        <p><strong>Notifications</strong></p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 p-0">
                                    <div class="table_header">
                                        <p><strong>Account Number</strong></p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 p-0">
                                    <div class="table_header">
                                        <p><strong>Date</strong></p>
                                    </div>
                                </div>
                                <!--End Table Header-->
                                <!--Table Content-->
                                <div class="col-md-6 col-sm-6 p-0">
                                    <div class="table_item">
                                        Your <span><a href="#" class="colr_text">Term Loan</a></span> Was Opened    Loan Payment
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 p-0">
                                    <div class="table_item">
                                        1866745442309553
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 p-0">
                                    <div class="table_item">
                                        3 months ago
                                    </div>
                                </div>
                                <!--End Table Content-->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="payment_method">
                        <h4>Auto Payments</h4>
                    </div>
                    <div class="auto_payment">
                        <p>your daily auto-payment of <strong>$204.60</strong>is scheduled for every day </p>
                    </div>
                    <div class="contact_sec">
                        <div class="photo_sec">
                            <img src="images/pic10.jpg" alt="No-Img" class="img-responsive">
                        </div>
                        <div class="contact_info">
                            <i class="fa fa-phone" aria-hidden="true"></i> <span>212.634.4272</span>
                            <div class="phone_contact">
                                <i class="fa fa-phone-square" aria-hidden="true"></i> <span>1-800-363-1160</span>
                            </div>
                            <i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="#" class="colr_text">abetancur@ondeck.com</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @stop