@extends('layouts.master')
@section('content')
<!-- Start Navigation-->
        <section class="navigation">
            <div class="container">
                <div class="card">
                    <ul class="sub_menu nav nav-tabs sub_menu" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Payment</a>
                        </li>
                        <li role="presentation">
                            <a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">View Payment</a>
                        </li>
                        <li><a href="#"><span class="">Confirm Payment</span></a></li>
                    </ul>
                </div>
            </div>
        </section>
        <!-- End Navigation-->

        <!-- start Body Container -->

        <!--crow_1_2-->
        <div class="body_color">
            <div class="container">
                <div class="row">
                    <div class="col-md-9 col-sm-8">
                        <div class="adjust_sec">
                            <div class="amount_sec">
                                <div class="col-md-12 col-sm-12 p-0">
                                    <div class="colr_text">
                                        <h4>Select payment amount</h4>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-12 p-0">
                                    <div class="col-md-8 col-sm-12 p-0">
                                        <div class="col-md-8 col-sm-12 p-0">
                                            <div class="payment_selection">
                                                <p class="m-b-0"><strong>Minimum Monthly Payment</strong></p>
                                                <p class="p-t-5">You currently have no payment due</p>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-12 clearfix p-0">
                                            <div class="payment_selection pull-right m-b-10">
                                                <h4><strong>$0.00</strong></h4>
                                            </div>
                                        </div>
                                        <div class="col-md-12 col-sm-12 p-0">
                                            <div class="radio_check">
                                                <input type="radio" name="amount" id="" value="amount" checked="checked">
                                                <p class="total_amount"> Entire Amount </p>
                                                <p class="pull-right remaning_amount"> $13,828.75</p>
                                            </div>
                                            <div class="radio_check">
                                                <input type="radio" name="amount" id="" value="other">
                                                <p class="total_amount"> Other Amount </p>
                                                <input type="text" name="" id="" class=" manual_amount pull-right">
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="payment_procedure_sec">
                                        <div class="payment_procedure">
                                            <p class="">Payment method</p>
                                        </div>
                                        <div class="payment_procedure">
                                            <p class="">PNC Bank-*6804</p>
                                        </div>
                                        <div class="col-md-12 col-sm-12 p-0">
                                            <div class="review_btn removing_width">
                                                <a href="#">Review and Pay
                                                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4">
                        <div class="right_sec sec_length">
                            <div class="col-md-12 col-sm-12 p-0">
                                <div class="balance_info solid_border">
                                    <span class="pull-left ligh_text">Account Details</span>
                                </div>
                                <div class="main_details no_border">
                                    <div class="further_account_detail">
                                        <span class="pull-left small_text">Available Funds</span>
                                        <span class="pull-right clr_black"> <strong>$5,700.00</strong></span>
                                    </div>
                                    <div class="further_account_detail">
                                        <span class="pull-left small_text">Outstanding Balance</span>
                                        <span class="pull-right clr_black"> <strong>$13,828.75</strong></span>
                                    </div>  
                                    <div class="further_account_detail">
                                        <span class="pull-left small_text">Amount Past Due</span>
                                        <span class="pull-right clr_black"><strong>$0.00</strong></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-8">
                        <div class="table_content m-t-30">
                            <div class="amount_sec">
                                <div class="col-md-12 col-sm-12">
                                    <div class="colr_text p-b-10">
                                        <h3>Payment schedule</h3>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 p-0">
                                    <!-- Nav tabs -->
                                    <div class="card">
                                        <ul class="nav nav-tabs tabs" role="tablist">
                                            <li role="presentation" class="active">
                                                <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Payment schedule</a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Details by loan</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="home">
                                        <div class="payment_chart">
                                            <table class="payment_table">
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Principal</th>
                                                    <th>Fee</th>
                                                    <th>Total Due</th>
                                                </tr>
                                                <tr>
                                                    <td>8/10/2017</td>
                                                    <td>$2,575.00</td>
                                                    <td>$1,142.25</td>
                                                    <td><strong>$3,717.25</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>9/10/2017</td>
                                                    <td>$2,575.00</td>
                                                    <td>$1,142.25</td>
                                                    <td><strong>$3,717.25</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>10/10/2017</td>
                                                    <td>$2,575.00</td>
                                                    <td>$252.00</td>
                                                    <td><strong>$2,827.00</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>11/10/2017</td>
                                                    <td>$2,575.00</td>
                                                    <td>$252.00</td>
                                                    <td><strong>$2,827.00</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>12/11/2017</td>
                                                    <td>$2,575.00</td>
                                                    <td>$252.00</td>
                                                    <td><strong>$2,827.00</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>01/10/2018</td>
                                                    <td>$2,575.00</td>
                                                    <td>$252.00</td>
                                                    <td><strong>$2,827.00</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>02/10/2018</td>
                                                    <td>$1,625.00</td>
                                                    <td>$195.00</td>
                                                    <td><strong>$1,820.00</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>03/10/2018</td>
                                                    <td>$1,625.00</td>
                                                    <td>$195.00</td>
                                                    <td><strong>$1,820.00</strong></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="profile">
                                        <div class="payment_chart">
                                            <table class="payment_table">
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Principal</th>
                                                    <th>Fee</th>
                                                    <th>Total Due</th>
                                                </tr>
                                                <tr>
                                                    <td>8/10/2017</td>
                                                    <td>$2,575.00</td>
                                                    <td>$1,142.25</td>
                                                    <td><strong>$3,717.25</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>9/10/2017</td>
                                                    <td>$2,575.00</td>
                                                    <td>$1,142.25</td>
                                                    <td><strong>$3,717.25</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>10/10/2017</td>
                                                    <td>$2,575.00</td>
                                                    <td>$252.00</td>
                                                    <td><strong>$2,827.00</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>11/10/2017</td>
                                                    <td>$2,575.00</td>
                                                    <td>$252.00</td>
                                                    <td><strong>$2,827.00</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>12/11/2017</td>
                                                    <td>$2,575.00</td>
                                                    <td>$252.00</td>
                                                    <td><strong>$2,827.00</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>01/10/2018</td>
                                                    <td>$2,575.00</td>
                                                    <td>$252.00</td>
                                                    <td><strong>$2,827.00</strong></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-4 m-b-15">
                        <div class="right_sec">
                            <ul class="question_sec">
                                <li class="colr_text">Frequently Asked Questions</li>
                                <li><a href="#" class="colr_text">How to loan payments works ?</a></li>
                                <li><a href="#" class="colr_text">How are my costs calculated ?</a></li>
                                <li><a href="#" class="colr_text">How can i change my deposit location ?</a></li>
                                <li><a href="#" class="colr_text">Are there prepayment panalties ?</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @stop