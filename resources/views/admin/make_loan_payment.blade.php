@extends('layouts.master')
@section('content')
<div class="term_bg_colr m-b-15">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="page_header">
                                <h1>Make a Term Loan Payment</h1>
                            </div>
                            <span class="balance_detail pull-right">$40,000.00 LOAN # 1866745442309553</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 p-0">
                        <div class="balance_inqury">
                            <h1><strong>$39,692.40</strong></h1>
                            <span class="amount_text">Current Balance</span>
                        </div>
                    </div>
                    <div class="col-md-12 col-sm-12 p-0">
                        <div class="paragraph">
                            <p><strong>This is an additional payment of your loan payment shedule and will apply to both your principal and interest balances. </strong></p>
                        </div>
                        <div class="light_text">
                            <p>We will continue to debit your account according to your loan agreement.</p>
                        </div>
                        <div class="paragraph p-b-20"><strong>For payments initiated Monday - Thursday before 12:30 am PKT, funds will be debited within 24 hours. For Funds drawn after 12:30am PKT or on a Fri/Sat/Sun or Bank Holidays, funds will be debited in two days.</strong></div>
                    </div>
                </div>
            </div>
            <div class="top_borders bottom_border">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 p-0">
                            <div class="payment_and_pass_sec">
                                <div class="col-md-6 col-sm-6">
                                    <form class="input_field">
                                        <div class="form-group">
                                            <h3> <strong>Payment Amount</strong></h3>
                                            <input type="email" name="email" id=" " value="abc"  class="form-control input_manual_length login_input m-0 " placeholder="">
                                        </div>
                                    </form>    
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <div class="solid_border">
                                        <h3 class="payment_text"><strong>Payment will be taken from Bank account </strong></h3>
                                    </div>
                                    <div class="password">
                                        <div class="light_text">
                                            <p>*****1803</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="payment_and_cancel_btn">
                        <div class="prew_btn">
                            <a href="#" class="btn_left brown_colr">Preview Payment</a>
                        </div>
                        <span class="m-l-10 m-r-10">or</span>
                        <div class="cancel_btn">
                            <a href="#">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop
        <!-- Start Footer-->