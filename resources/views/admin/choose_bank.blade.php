@extends('layouts.master')
@section('content')
<div class="term_bg_colr m-b-15">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="page_header">
                            <h1>Who do you bank with?</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="company_account">
                        <p>Please select the bank where you have<strong> your company’s operating account.</strong></p>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12">
                    <ul class="banks_branches">
                        <li id="chose_bank">
                            <a href="#">
                                <input type="radio" name="bank_input" class="bank_selector" value="chase">
                                <img src="images/chase.png" alt="No-img"></a>
                        </li>
                        <li id="chose_bank">
                            <a href="#">
                                <input type="radio" name="bank_input" class="bank_selector" value="america" >
                                <img src="images/america_bank.png" alt="No-img">
                            </a>
                        </li>
                        <li id="chose_bank">
                            <a href="#">
                                <input type="radio" name="bank_input" class="bank_selector" value="wells" >
                                <img src="images/wells.png" alt="No-img">
                            </a>
                        </li>
                        <li id="chose_bank">
                            <a href="#">
                                <input type="radio" name="bank_input" class="bank_selector" value="pnc">
                                <img src="images/pnc.png" alt="No-img">
                            </a>
                        </li>
                        <li id="chose_bank">
                            <a href="#">
                                <input type="radio" name="bank_input" class="bank_selector" value="us_bank" >
                                <img src="images/us_bank.png" alt="No-img">
                            </a>
                        </li>
                        <li id="other_bank" style="cursor:pointer">

                            <input type="radio" name="bank_input" id="show_input"  value="other" >Other...
                        </li>
                    </ul>
                </div>

                <div class="col-md-12 col-sm-12 hide_input_sec">
                    <div class="input-group input_length" id="">
                        <span><strong>Bank Name</strong></span>
                        <input type="text" class="form-control input_height">
                        <p><a href="#" class="colr_text">Let us know </a>if you can’t find your bank.</p>
                    </div>
                </div>
                <div class="continue_btn">
                    <a href="#" class="bank_continue_btn">Continue</a>
                </div>
                <div class="bank_select_btn">
                    <a href="#" class="btn_left brown_colr">Please selecr a bank</a>
                </div>
                <!-- chat Box-->
                <div class="row chat-window col-xs-5 col-md-3" id="chat_window" style="">
                    <div class="col-xs-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading top-bar">
                                <div class="col-md-8 col-xs-8">
                                    <h3 class="panel-title white_clr">Chat with us </h3>
                                </div>
                                <div class="col-md-4 col-xs-4" style="text-align: right;">
                                    <a href="#"><span id="minim_chat_window" class="icon_minim white_clr"><i class="minimize_icon fa fa-minus" aria-hidden="true"></i></span></a>
                                    <a href="#"><span class="icon_close white_clr" data-id="chat_window_1"><i class="fa fa-times" aria-hidden="true"></i></span></a>
                                </div>
                            </div>
                            <div class="panel-body msg_container_base">
                                <form>
                                    <div class="inputs">
                                        <div class="input-group date" class="datetimepicker1">
                                            <span>Your Name*</span>
                                            <input type="text" required value="" placeholder="Enter text here" class="form-control input_height">
                                        </div>
                                    </div>
                                    <div class="inputs">
                                        <div class="input-group date" class="datetimepicker1">
                                            <span>Phone Number*</span>
                                            <input type="tel" required value="" placeholder="Enter text here" class="form-control input_height">
                                        </div>
                                    </div>
                                    <div class="inputs">
                                        <div class="input-group date" class="datetimepicker1">
                                            <span>Email Address</span>
                                            <input type="text" required value="" placeholder="Enter text here" class="form-control input_height">
                                        </div>
                                    </div>
                                    <div class="inputs">
                                        <div class="input-group date" class="datetimepicker1">
                                            <span>Business Name</span>
                                            <input type="text" required value="" placeholder="Enter text here" class="form-control input_height">
                                        </div>
                                    </div>
                                    <div class="best_reason">
                                        <p>Which option best describes your reason for contacting OnDeck?*</p>
                                    </div>
                                    <div class="options">
                                        <select>
                                            <option class="-1">Choose item from the list </option>
                                            <option value="0">
                                                I am a business owner and want information about OnDeck loans.</option>
                                            <option value="1">
                                                I’ve already submitted a loan application, and want to check on the status.</option>
                                            <option value="2">
                                                I have not yet started a business and want information about OnDeck loans.</option>
                                            <option value="3">
                                                I am a current OnDeck customer and have questions about my account.</option>
                                            <option value="4">
                                                I want to become an OnDeck partner or already am an OnDeck partner.</option>
                                            <option value="5">
                                                I have questions about payments on my OnDeck loan.
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form_btns">
                                        <a href="#" class="deny_btn">Cancel</a>
                                        <input type="button" value="submit">
                                        <div class="bottom_space"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Chat Box-->
        </div>
@stop