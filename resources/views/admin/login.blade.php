<!DOCTYPE html>
<html lang="en" class="login_bg">
   <head>
      <meta charset="UTF-8">
      <meta name="_token" content="{{ csrf_token() }}" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Log in Page</title>
      <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
      <link rel="stylesheet" href="css/font-awesome.min.css">
      <link rel="stylesheet" href="css/bootstrap.min.css">
      <link rel="stylesheet" href="css/style.css">
   </head>
   <body>
      <header>
         <!--start pre Header-->
         <div class="pre_header">
            <div class="container">
               <ul class="pre_header_list pull-right">
                  <li><a href="#">chat live</a></li>
                  <li><strong>Call Us:</strong>1.888 726.2198</li>
               </ul>
            </div>
         </div>
         <!--End pre Header-->
      </header>
      <!--Star Main Header -->
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-12 12">
                    <div class="logo">
                        <img src="images/other_logo.jpg" alt="No-Img" class="img-responsive">
                    </div>
                </div>
                <!--End Main Header-->

                <!--Sign Up Page-->
                <div class="col-md-3 col-sm-12">
                    <div class="payment_btn m-t-10 reg_btn">
                        <a href="#" class="btn_left pull-right" data-toggle="modal" data-target="#myModal">Register Now</a>
                    </div>
                </div>
                <!-- Start Popup -->
                <div class="modal fade main_popup_sec" id="myModal" role="dialog">
                    <div class="modal-dialog popup_width">
                        <!-- Popup content-->
                        <div class="modal-content">
                            <div class="modal-header no_border p-0">

                                <div class="col-md-12 col-sm-12 p-0">
                                    <div class="sing_up_header m-b-30">
                                        <h2>Signup Now</h2>
                                        <button type="button" class="close cross_icon" data-dismiss="modal">&times;</button>
                                        <form role="form" action="javascript:;" method="post" id="login-form" autocomplete="off"></form>
                                    </div>

                                </div>
                                <form role="form" data-toggle="validator" method="post " id="registerUser" class="p-l-r-10 p-t-10 p-b-10 clearfix">
                                    <div class="abc">
                                        <div class="form-group m-0 clearfix">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="form-group m-b-20">
                                                    <input type="text" name="name" id="name" class="form-control reg_input " placeholder="Your Name" data-error="Name is required." required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group m-b-20">
                                                    <input type="email" name="email" id="email" class="form-control reg_input " placeholder="Your Email" data-error="Email address is required" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group m-b-20">
                                                    <input type="password" name="password" id="inputPassword" class="form-control reg_input " placeholder="Password" data-minlength="6" data-error="Password is required with minimum 6 characters." required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="form-group m-b-20">
                                                    <input type="password" name="confirm_password" id="confirm_passed" class="form-control reg_input " placeholder="Confirm Password" data-match="#inputPassword" data-error="Password does not match." required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="login_btn m-b-20">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }} ">
                                        <input type="submit" id="btn-login" class="btn sign_up_btn btn-lg btn-block" value="Sign Up">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Sign Up Page-->

        <!--Log In page-->
        <div class="top_borders">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-wrap">
                            <div class="alert alert-danger alert-block fade in error_message">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <i class="fa fa-ok-sign"></i>
                                <div class="error_text">
                                    <span><strong>Oh snap!</strong> Change a few things up and try submitting again.</span>
                                </div>
                            </div>
                            <div class="alert alert-success alert-block fade in error_message">
                                <button data-dismiss="alert" class="close close-sm" type="button">
                                    <i class="fa fa-times"></i>
                                </button>
                                <i class="fa fa-ok-sign"></i>
                                <div class="error_text">
                                    <span><strong>Oh snap!</strong> Change a few things up and try submitting again</span>
                                </div>
                            </div>
                            <form role="form" action="/dashboard" data-toggle="validator"  method="get" id="login-form" autocomplete="off">
                                <div class="form-group">
                                    <input type="email" name="email" id="email" class="form-control login_input input_height small_view" placeholder="Email Address" data-error="Valid email address required." required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="key" id="key" class="form-control login_input input_height small_view "  placeholder="Password" data-minlength="6" data-error="Password is required with minimum 6 characters." required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="login_btn">
                                    <input type="submit" id="btn-login" class="btn btn-custom btn-lg btn-block" value="Log in">
                                    <a href="javascript:;" class="forget colr_text" data-toggle="modal" data-target=".forget-modal">Forgot password?</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--End Log In page-->
      <script src="js/jquery.min.js"></script>
      <script src="js/bootstrap.min.js"></script>
      <script src="js/validator.js"></script>
      <script src="js/login.js"></script>
   </body>
</html>