@extends('layouts.master')
@section('content')
<!-- Start Navigation-->
        <div class="main_content">
            <section class="navigation">
                <div class="container">
                    <div class="card">
                        <ul class="sub_menu nav nav-tabs underline" role="tablist">
                            <li role="presentation" class="active">
                                <a href="#Overview_page" aria-controls="loan_page" role="tab" data-toggle="tab"><span class="caret_icon">Overview</span></a>
                            </li>
                            <li role="presentation">
                                <a href="#team_loan_page" aria-controls="loan_page" role="tab" data-toggle="tab"><span class="">Current Loans</span></a>
                            </li>
                            <li role="presentation">
                                <a href="#def" aria-controls="loan_page" role="tab" data-toggle="tab"><span class="">Past Loans</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
            <!-- End Navigation-->
            <!-- start Body Container -->
            <!--crow_1_2-->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="Overview_page">
                    <div class="body_color">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="account_details">
                                        <label  class="pull-left"> <h1>Create New Loan</h1></label>
                                    </div>

                                </div>
                                <div class="col-md-9 col-sm-8">
                                    <div class="adjust_sec">
                                        <div class="amount_sec">
                                            <div class="col-md-12 col-sm-12">
                                                <span>
                                                    <p>Welcome back, {{ ucwords(Auth::user()->name) }} !</p>
                                                </span>
                                            </div>
                                             <form action="" method="postS">
                                                <div class="col-md-12 col-sm-12 p-0 funds">
                                                    <div class="col-md-8 col-sm-12 p-0">
                                                        <div class="colr_text">
                                                            <h3>How much would you like to lend today?</h3>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12 p-0">
                                                        <div class="account_info">
                                                            <span class="amount_details pull-right">Available Interest to Re-Lend <strong>$1,700.05</strong></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 p-0 ">
                                                    <div class="col-md-8 col-sm-12 ">
                                                    <div class="amout_manual_sec clearfix">
                                                        <select class="loan_dropdown_ds">
                                                            <option>$1,000</option>
                                                            <option>$2,000</option>
                                                            <option>$3,000</option>
                                                            <option>$4,000</option>
                                                            <option>$5,000</option>
                                                            <option>$6,000</option>
                                                            <option>$7,000</option>
                                                            <option>$8,000</option>
                                                            <option>$9,000</option>
                                                            <option>$10,000</option>
                                                        </select>
                                                        <div class="col-md-12 col-sm-12 p-0 exact_input_field" style="display: none;">
                                                    <div class=" amount_input p-t-8">
                                                        <input type="text" class="form-control input_height" name="entered_amount" id="entered_amount" title="Minimum value should be 10K" min="1000"  pattern="[1-8][0-9]" required>

                                                    </div>
                                                </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12 p-0">
                                                    <div class="radio_check p-t-10">
                                                        <input type="checkbox" name="amount_enter_checkbox" id="amount_enter_checkbox">
                                                        <span class="colr_text">Enter exact amount</span>
                                                    </div>
                                                </div>
                                                    </div>
                                                    <div class="col-md-4 col-sm-12">
                                                        <div class="btn_holder">
                                                            <input type="submit" id="btn-login" class="btn btn-lg btn-block" value="Review Terms"> <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                                
                                            </form>
                                        </div>
                                    </div>
                                    <!-- Crow_1_1-->
                                    <div class="loan_details">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="col-md-4 col-sm-12 p-0">
                                                    <div class="left_transaction">
                                                        <div class="amount_summary">
                                                            <span class="total_amout">$10,230.00</span>
                                                            <p class="amount_sumary">Interest Earned on All Loans to date</p>
                                                        </div>
                                                        <div class="transaction_btn">
                                                            <a href="#" class="btn_left">Transaction History</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6 p-0">
                                                    <div class="left_transaction">
                                                        <div class="amount_summary">
                                                            <span class="total_amout">$300.00</span>
                                                            <p class="amount_sumary">Interest Available to Withdraw</p>
                                                        </div>
                                                        <div class="transaction_btn">
                                                            <a href="#" class="btn_left">Withdraw Interest</a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-6 p-0">
                                                    <div class="right_transaction">
                                                        <div class="amount_summary">
                                                            <span class="total_amout">$41,329.20</span>
                                                            <p class="amount_sumary">Total Outstanding Principal</p>
                                                        </div>
                                                        <div class="transaction_btn">
                                                            <a href="#" class="btn_left">View Loans</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--End Crow_1_1-->
                                </div>
                                <div class="col-md-3 col-sm-4">
                                    <div class="adjust_sec">
                                        <div class="amount_sec">
                                            <div class="col-md-12 col-sm-12">
                                                <div class="counter_header">
                                                    <span class="counter_text"><b>Months to Next Maturity</b></span>
                                                    <span class="balance_detail">
                                                    Select Loan
                                                    <select id="loans_dropdown" name="Current Loans" size="1" >
                                                    @foreach($loans as $loan)
                                                        <option value="{{$loan->id}}" selected="selected">${{$loan->amount}} LOAN # ({{$loan->id}}) CREATED: {{$loan->created_at}}</option>
                                                    @endforeach  
                                                    </select>
                                                </span>
                                                </div>


                                            </div>
                                            <div class="col-md-12 col-sm-12">
                                                <div id="progress"></div>
                                            </div>
                                            <div class="col-md-12 col-sm-12 p-0">
                                                <div class="balance_info">
                                                    <span class="pull-left ligh_text">Interest</span>
                                                    <span class="pull-right amount">$3,828 <small>.75</small></span>
                                                </div>
                                                <div class="main_details">
                                                    <div class="further_account_detail">
                                                        <span class="pull-left small_text">Next Statement Date</span>
                                                        <span class="pull-right small_text"> 09/01/2017</span>
                                                    </div>
                                                    <div class="further_account_detail">
                                                        <!-- Payment date will be 30 days from origination date -->
                                                        <span class="pull-left small_text">Next Payment Date</span>
                                                        <span class="pull-right small_text">09/05/2017</span>
                                                    </div>
                                                    <div class="further_account_detail">
                                                        <span class="pull-left small_text">Payment Amount</span>
                                                        <span class="pull-right small_text">$50.00</span>
                                                    </div>
                                                    <div class="further_account_detail">
                                                        <span class="pull-left small_text">Principal Balance</span>
                                                        <span class="pull-right small_text">$5,000.00</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9 col-sm-8">
                                <div class="notification_details">
                                    <span class="colr_text pull-right p-t-15"><b>Total projected interest on all loans this month: $750.00</b></span>
                                    <div class="payment_method">
                                        <h4>Auto Draft</h4>
                                    </div>
                                    <div class="">
                                        <p>Your current auto draft is set to withdraw <strong>$150 weekly on Fridays</strong> from bank account ending in <b>1234</b></p>
                                    </div>
                                    <div class="payment_method">
                                        <h4>Notification</h4>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12 p-0">
                                    <!-- Table Header-->
                                    <div class="table_main_sec m-t-15">
                                        <div class="col-md-6 col-sm-5 p-0">
                                            <div class="table_header">
                                                <p>Notification</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-4 p-0">
                                            <div class="table_header">
                                                <p>Account Number</p>
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 p-0">
                                            <div class="table_header">
                                                <p>Date</p>
                                            </div>
                                        </div>
                                        <!--End Table Header-->
                                        <!--Table Content-->
                                        <div class="col-md-6 col-sm-5 p-0">
                                            <div class="table_item">
                                                Your <span><a href="#" class="colr_text">Term Loan</a></span> was opened
                                            </div>
                                            <div class="table_item">
                                                Your <span><a href="#" class="colr_text">Term Loan</a></span> was opened
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-4 p-0">
                                            <div class="table_item">
                                                1866745442309553
                                            </div>
                                            <div class="table_item">
                                                1637064799355838
                                            </div>
                                        </div>
                                        <div class="col-md-3 col-sm-3 p-0">
                                            <div class="table_item">
                                                2 months ago
                                            </div>
                                            <div class="table_item">
                                                9 months ago
                                            </div>
                                        </div>
                                        <!--End Table Content-->
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-4 m-b-15">
                                <div class="right_sec">
                                    <ul class="question_sec">
                                        <li class="colr_text">Frequently Asked Questions</li>
                                        <li><a href="#" class="colr_text">How to loan payments works ?</a></li>
                                        <li><a href="#" class="colr_text">How are my costs calculated ?</a></li>
                                        <li><a href="#" class="colr_text">How can i change my deposit location ?</a></li>
                                        <li><a href="#" class="colr_text">Are there prepayment panalties ?</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="team_loan_page">
                    <div class="term_bg_colr">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="page_header">
                                        <!-- Need to have this information updated from the chosen drop down -->
                                        <h1 class="chosen_loan">Select a Loan</h1>
                                    </div>
                                    <span class="balance_detail pull-right">
                                        Select Loan
                                        <select id="current_loans" name="Current Loans" size="1">
                                        @foreach($loans as $loan)
                                            <option value="{{$loan->id}}" selected="selected">${{$loan->amount}} LOAN # ({{$loan->id}}) CREATED: {{date('d-m-Y',strtotime($loan->created_at))}}</option>
                                        @endforeach  
                                        </select>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="Term_loan_top_sec">
                                    <div class="col-md-4 col-sm-4">
                                        <div class="border_right">
                                            <div class="col-md-4 col-sm-4">
                                                <div class="total_balance_sec">
                                                    <div id="term"></div>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-sm-8 p-0">
                                                <div class="remaining_amount">
                                                    <h2>$40,306.20</h2>
                                                    <p>Outstanding balance of <strong>$51,559.20 Loan</strong></p>
                                                </div>
                                                <div class="payment_btn">
                                                    <a href="make-payment">Make a Payment</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="border_right p-r-30">
                                            <div class="col-md-4 col-sm-4 p-0">
                                                <div class="clander_icon">
                                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-sm-8 p-0">
                                                <div class="date_holder">
                                                    <h2>May 4th, 2018</h2>
                                                    <p>Approximate payoff date</p>
                                                </div>
                                                <div class="shedule_btn">
                                                    <a href="#" data-toggle="modal" data-target="#myModal">Download Amortization Schedule</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-4 p-0">
                                        <div class="">
                                            <div class="col-md-12 col-sm-12 p-0">
                                                <div class="clander_icon">
                                                    <small>REMAINING PAYMENTS</small>
                                                    <h2>197</h2>
                                                    <div class="loan_text">
                                                        <small>LOAN PERIOD</small>
                                                        <h2>36 Months</h2>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8 col-sm-8">
                                <!-- Nav tabs -->
                                <div class="card">
                                    <ul class="nav nav-tabs tabs" role="tablist">
                                        <li role="presentation" class="active">
                                            <a href="#home" aria-controls="home" role="tab" data-toggle="tab">All Transaction History</a>
                                        </li>
                                        <li role="presentation">
                                            <a href="#notification" aria-controls="profile" role="tab" data-toggle="tab">Notification</a>
                                        </li>
                                    </ul>
                                </div>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="home">
                                        <div class="table_main_sec">
                                            <!--Table Header-->
                                            <div class="col-md-2 col-sm-2">
                                                <div class="table_header">
                                                    <p>Date</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2">
                                                <div class="table_header">
                                                    <p>Type</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2">
                                                <div class="table_header">
                                                    <p>Status</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2">
                                                <div class="table_header">
                                                    <p>Amount</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2">
                                                <div class="table_header">
                                                    <p>Principal</p>
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2">
                                                <div class="table_header">
                                                    <p>Interest</p>
                                                </div>
                                            </div>
                                            <!--End Table Header-->
                                            <!-- Table Content-->
                                            <div class="col-md-2 col-sm-2">
                                                <div class="table_item">
                                                    07/21/2017
                                                </div>
                                                <div class="table_item">
                                                    07/10/2017
                                                </div>
                                                <div class="table_item">
                                                    07/19/2017
                                                </div>
                                                <div class="table_item">
                                                    07/18/2017
                                                </div>
                                                <div class="table_item">
                                                    07/17/2017
                                                </div>
                                                <div class="table_item">
                                                    07/14/2017
                                                </div>
                                                <div class="table_item">
                                                    07/13/2017
                                                </div>
                                                <div class="table_item">
                                                    07/12/2017
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2">
                                                <div class="table_item">
                                                    Loan Payment
                                                </div>
                                                <div class="table_item">
                                                    Loan Payment
                                                </div>
                                                <div class="table_item">
                                                    Loan Payment
                                                </div>
                                                <div class="table_item">
                                                    Loan Payment
                                                </div>
                                                <div class="table_item">
                                                    Loan Payment
                                                </div>
                                                <div class="table_item">
                                                    Loan Payment
                                                </div>
                                                <div class="table_item">
                                                    Loan Payment
                                                </div>
                                                <div class="table_item">
                                                    Loan Payment
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2">
                                                <div class="table_item">
                                                    Pending
                                                </div>
                                                <div class="table_item">
                                                    Completed
                                                </div>
                                                <div class="table_item">
                                                    Completed
                                                </div>
                                                <div class="table_item">
                                                    Completed
                                                </div>
                                                <div class="table_item">
                                                    Completed
                                                </div>
                                                <div class="table_item">
                                                    Completed
                                                </div>
                                                <div class="table_item">
                                                    Completed
                                                </div>
                                                <div class="table_item">
                                                    Completed
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2">
                                                <div class="table_item">
                                                    $204.60
                                                </div>
                                                <div class="table_item">
                                                    $204.60
                                                </div>
                                                <div class="table_item">
                                                    $204.60
                                                </div>
                                                <div class="table_item">
                                                    $204.60
                                                </div>
                                                <div class="table_item">
                                                    $204.60
                                                </div>
                                                <div class="table_item">
                                                    $204.60
                                                </div>
                                                <div class="table_item">
                                                    $204.60
                                                </div>
                                                <div class="table_item">
                                                    $204.60
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-sm-2">
                                                <div class="table_item">
                                                    $134.75
                                                </div>
                                                <div class="table_item">
                                                    $134.47
                                                </div>
                                                <div class="table_item">
                                                    $134.19
                                                </div>
                                                <div class="table_item">
                                                    $133.91
                                                </div>
                                                <div class="table_item">
                                                    $133.62
                                                </div>
                                                <div class="table_item">
                                                    $133.34
                                                </div>
                                                <div class="table_item">
                                                    $133.06
                                                </div>
                                                <div class="table_item">
                                                    $132.79
                                                </div>

                                            </div>
                                            <div class="col-md-2 col-sm-2">
                                                <div class="table_item">
                                                    $70.13
                                                </div>
                                                <div class="table_item">
                                                    $69.85
                                                </div>
                                                <div class="table_item">
                                                    $70.41
                                                </div>
                                                <div class="table_item">
                                                    $70.69
                                                </div>
                                                <div class="table_item">
                                                    $70.98
                                                </div>
                                                <div class="table_item">
                                                    $71.26
                                                </div>
                                                <div class="table_item">
                                                    $71.54
                                                </div>
                                                <div class="table_item">
                                                    $71.81
                                                </div>
                                            </div>
                                            <!--End Table Content-->
                                        </div>
                                        <div class="col-md-12 col-sm-12 p-0">
                                            <div class="table_detalis">
                                                <a href="#" class="colr_text">Show More <i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                                <a href="#" class="colr_text pull-right"> <i class="fa fa-download" aria-hidden="true"></i> Download CSV</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="notification">
                                        <div class="table_main_sec">
                               <!-- Table Header-->
                                <div class="col-md-6 col-sm-6 p-0">
                                    <div class="table_header">
                                        <p><strong>Notifications</strong></p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 p-0">
                                    <div class="table_header">
                                        <p><strong>Account Number</strong></p>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 p-0">
                                    <div class="table_header">
                                        <p><strong>Date</strong></p>
                                    </div>
                                </div>
                                <!--End Table Header-->
                                <!--Table Content-->
                                <div class="col-md-6 col-sm-6 p-0">
                                    <div class="table_item">
                                        Your <span><a href="#" class="colr_text">Term Loan</a></span> Was Opened    Loan Payment
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 p-0">
                                    <div class="table_item">
                                        1866745442309553
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3 p-0">
                                    <div class="table_item">
                                        3 months ago
                                    </div>
                                </div>
                                <!--End Table Content-->
                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4">
                                <div class="payment_method">
                                    <h4>Auto Payments</h4>
                                </div>
                                <div class="auto_payment">
                                    <p>your daily auto-payment of <strong>$204.60</strong>is scheduled for every day </p>
                                </div>
                                <div class="contact_sec">
                                    <div class="photo_sec">
                                        <img src="images/pic10.jpg" alt="No-Img" class="img-responsive">
                                    </div>
                                    <div class="contact_info">
                                        <i class="fa fa-phone" aria-hidden="true"></i> <span>212.634.4272</span>
                                        <div class="phone_contact">
                                            <i class="fa fa-phone-square" aria-hidden="true"></i> <span>1-800-363-1160</span>
                                        </div>
                                        <i class="fa fa-envelope-o" aria-hidden="true"></i> <a href="#" class="colr_text">abetancur@ondeck.com</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div role="tabpanel" class="tab-pane" id="def">
                    <div class="term_bg_colr m-b-15">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <div class="page_header">
                                        <h1>Past Loans</h1>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="page_height">
                                <div class="table_main_sec">
                                    <!-- Table Header-->
                                    <div class="col-md-5 col-sm-5 p-0">
                                        <div class="table_header">
                                            <p><strong>Loan</strong></p>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2 p-0">
                                        <div class="table_header">
                                            <p><strong> Date Created</strong></p>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2 p-0">
                                        <div class="table_header">
                                            <p><strong>Status</strong></p>
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 p-0">
                                        <div class="table_header">
                                            <p><strong>Loan ID</strong></p>
                                        </div>
                                    </div>
                                    <!--End Table Header-->

                                    <!--Table Content-->
                                    <div class="col-md-5 col-sm-5 p-0">
                                        <div class="table_item">
                                            <span><a href="term-loan-hold" class="colr_text">$40,000.00 36 Month Term Loan</a></span>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2 p-0">
                                        <div class="table_item">
                                            10/20/1016
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-2 p-0">
                                        <div class="table_item">
                                            Completed
                                        </div>
                                    </div>
                                    <div class="col-md-3 col-sm-3 p-0">
                                        <div class="table_item">
                                            1637064799358538
                                        </div>
                                    </div>
                                    <!--End Table Content-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End crow_1_2-->
        <!-- Start Popup -->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog popup_width">

                <!-- Popup content-->
                <div class="modal-content heading_center">
                    <div class="modal-header no_border p-0">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h3 class="modal-title p-b-10">
                            <strong>Your download has begun...</strong>
                        </h3>
                        <i class="fa fa-cloud-download" aria-hidden="true"></i>
                    </div>
                    <div class="modal-body p-0">
                        <p>The schedule reflects your loan terms and payment schedule at the time your loan was funded. The actual amortization may differ due to several factors, including without limitation, changes in the origination date, state and federal bank holidays, missed payments, early payments, the existence of a prior outstanding loan, and other terms</p>

                        <p>Please refer to the transaction history on the page below for actual payment activity.</p>

                        <p>Do not use this schedule to predict a future payoff or to pay off your loan. For all payoff requests, please contact OnDeck Customer Service.</p>
                    </div>
                    <div class="payment_btn update_btn remaning_amount">
                        <a href="#">I Understand</a>
                    </div>
                </div>
            </div>
        </div>
        <!--End Popup-->
@stop