@extends('layouts.master')
@section('content')
<div class="term_bg_colr m-b-15">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 p-0">
                        <div class="page_header">
                            <h1>Update Your Account</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 p-0">
                    <div class="mail_sec">
                        <h3>Your Email Address</h3>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 p-0">
                    <form class="input_field">
                        <div class="form-group">

                            <input type="email" name="email" id="email" value="msn@gmail.com"  class="form-control inline login_input m-0 input_height" placeholder=""><a href="#" onclick="editEmail()" id="account_email">(edit)</a>
                        </div>
                        <div class="mail_sec">
                            <h3>Your OnDeck Password</h3>
                        </div>
                        <div class="form-group input_field">
                            <input type="password" name="email" id="pass" value="123456" class="form-control inline login_input m-0 input_height" placeholder=""><a href="#" onclick="editPassword()" id="account_pass">(edit)</a>

                        </div>
                    </form>
                </div>
                <div class="col-md-12 col-sm-12 p-0">
                    <div class="payment_btn update_btn">
                        <a href="#">Update</a> <span class="p-l-10 p-r-10">or</span>

                        <div class="shedule_btn update_btn">
                            <a href="#">Cancel</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 p-0">
                    <div class="payment_method">
                        <h5><strong>Other Settings</strong></h5>
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 p-0">
                    <div class="connect_btn">
                        <a href="choose-bank"> <strong>Connect Your Bank Account </strong> <i class="fa fa-lock" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
@stop