@extends('layouts.master')
@section('content')
<div class="term_bg_colr m-b-15">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="page_header">
                            <h1>Documents</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 p-0">
                    <div class="table_main_sec">
                        <div class="col-md-3 col-sm-12 col-xs-12 p-0">
                            <div class="table_header">
                                <span>Type</span>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-xs-12 p-0">
                            <div class="table_header">
                                <span>Name</span>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12 p-0">
                            <div class="table_header">
                                <span>Date</span>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12 p-0">
                            <div class="table_header">
                                <span>Product ID</span>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-12 col-xs-12 p-0">
                            <div class="table_item">
                                <span>Monthly Statement - Jun 2017</span>
                            </div>
                            <div class="table_item">
                                <span>Monthly Statement - May 2017</span>
                            </div>
                            <div class="table_item">
                                <span>Monthly Statement - May 2017</span>
                            </div>
                            <div class="table_item">
                                <span>Monthly Statement - Apr 2017</span>
                            </div>
                            <div class="table_item">
                                <span>Contract</span>
                            </div>
                            <div class="table_item">
                                <span>Monthly Statement - Mar 2017</span>
                            </div>
                            <div class="table_item">
                                <span>Monthly Statement - Feb 2017</span>
                            </div>
                            <div class="table_item">
                                <span>Monthly Statement - Jan 2017</span>
                            </div>
                            <div class="table_item">
                                <span>2016 Tax Statement</span>
                            </div>
                            <div class="table_item">
                                <span>Monthly Statement - Dec 2016</span>
                            </div>
                            <div class="table_item">
                                <span>Monthly Statement - Nov 2016</span>
                            </div>
                            <div class="table_item">
                                <span>Monthly Statement - Oct  2016</span>
                            </div>
                            <div class="table_item">
                                <span>Contract</span>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-12 col-xs-12 p-0">
                            <div class="table_item">
                                <span><a href="#" class="colr_text">Term Loan Monthly Statement - Jun 2017 (PDF)</a></span>
                            </div>
                            <div class="table_item">
                                <span><a href="#" class="colr_text">Term Loan Monthly Statement - May 2017 (PDF)</a></span>
                            </div>
                            <div class="table_item">
                                <span><a href="#" class="colr_text">Term Loan Monthly Statement - May 2017 (PDF)</a></span>
                            </div>
                            <div class="table_item">
                                <span><a href="#" class="colr_text">Term Loan Monthly Statement - Apr 2017 (PDF)</a></span>
                            </div>
                            <div class="table_item">
                                <span><a href="#" class="colr_text">Term Loan Contract (PDF)</a></span>
                            </div>
                            <div class="table_item">
                                <span><a href="#" class="colr_text">Term Loan Monthly Statement - Mar 2017 (PDF)</a></span>
                            </div>
                            <div class="table_item">
                                <span><a href="#" class="colr_text">Term Loan Monthly Statement - Feb 2017 (PDF)</a></span>
                            </div>
                            <div class="table_item">
                                <span><a href="#" class="colr_text">Term Loan Monthly Statement - Jan 2017 (PDF)</a></span>
                            </div>
                            <div class="table_item">
                                <span><a href="#" class="colr_text">Term Loan 2016 Tax Statement (PDF)</a></span>
                            </div>
                            <div class="table_item">
                                <span><a href="#" class="colr_text">Term Loan Monthly Statement - Dec 2016 (PDF)</a></span>
                            </div>
                            <div class="table_item">
                                <span><a href="#" class="colr_text">Term Loan Monthly Statement - Nov 2016 (PDF)</a></span>
                            </div>
                            <div class="table_item">
                                <span><a href="#" class="colr_text">Term Loan Monthly Statement - Oct  2016 (PDF)</a></span>
                            </div>
                            <div class="table_item">
                                <span><a href="#" class="colr_text">Term Loan Contract (PDF)</a></span>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12 p-0">
                            <div class="table_item">
                                <span>07/11/2017</span>
                            </div>
                            <div class="table_item">
                                <span>06/08/2017</span>
                            </div>
                            <div class="table_item">
                                <span>06/08/2017</span>
                            </div>
                            <div class="table_item">
                                <span>05/08/2017</span>
                            </div>
                            <div class="table_item">
                                <span>05/08/2017</span>
                            </div>
                            <div class="table_item">
                                <span>04/08/2017</span>
                            </div>
                            <div class="table_item">
                                <span>03/08/2017</span>
                            </div>
                            <div class="table_item">
                                <span>02/08/2017</span>
                            </div>
                            <div class="table_item">
                                <span>02/08/2017</span>
                            </div>
                            <div class="table_item">
                                <span>01/08/2017</span>
                            </div>
                            <div class="table_item">
                                <span>12/08/2017</span>
                            </div>
                            <div class="table_item">
                                <span>11/08/2017</span>
                            </div>
                            <div class="table_item">
                                <span>10/20/2016</span>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-12 col-xs-12 p-0">
                            <div class="table_item">
                                <span>1866745442309553</span>
                            </div>
                            <div class="table_item">
                                <span>1637064799358538</span>
                            </div>
                            <div class="table_item">
                                <span>1866745442309553</span>
                            </div>
                            <div class="table_item">
                                <span>1637064799358538</span>
                            </div>
                            <div class="table_item">
                                <span>1866745442309553</span>
                            </div>
                            <div class="table_item">
                                <span>1637064799358538</span>
                            </div>
                            <div class="table_item">
                                <span>1637064799358538</span>
                            </div>
                            <div class="table_item">
                                <span>1637064799358538</span>
                            </div>
                            <div class="table_item">
                                <span>1637064799358538</span>
                            </div>
                            <div class="table_item">
                                <span>1637064799358538</span>
                            </div>
                            <div class="table_item">
                                <span>1637064799358538</span>
                            </div>
                            <div class="table_item">
                                <span>1637064799358538</span>
                            </div>
                            <div class="table_item">
                                <span>1637064799358538</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@stop