@extends('layouts.master')
@section('content')
<div class=" top_borders bg-clr">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-sm-12 p-0 m-b-30">
                        <div class="content_header">
                            <h4 class=" mail_sec"><strong>General FAQs</strong></h4>
                        </div>
                        <div class="panel-group popup_main_sec" id="faqAccordion">
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question0">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>Who is OnDeck?</h3></a>
                                    </div>

                                </div>
                                <div id="question0" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>OnDeck is a technology company that is making the borrowing process simple, fast, and friction-free. To date, we've delivered over $1 billion nationwide.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question1">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>What are the amounts and terms of an OnDeck loan?</h3></a>
                                    </div>

                                </div>
                                <div id="question1" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>OnDeck offers loans ranging from $5,000 - $250,000, with terms between 3 - 18 months. For loans up to $35,000 you can complete the process entirely online. For loans above $35,000, we require you to speak with one of our Loan Specialists.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question2">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>Does OnDeck offer merchant cash advances or true small business loans?</h3></a>
                                    </div>

                                </div>
                                <div id="question2" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>OnDeck offers true business loans with fixed terms and payments - typically up to 50% less expensive than a merchant cash advance. Also, OnDeck reports your payment history to the credit agencies, whereas there are no credit benefits to using a merchant cash advance.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question3">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>What can I use my loan for?</h3></a>
                                    </div>

                                </div>
                                <div id="question3" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>Many small businesses use OnDeck financing for purchasing inventory, equipment, launching a marketing campaign, hiring additional employees or even general working capital.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question4">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>How are OnDeck loans repaid?</h3></a>
                                    </div>

                                </div>
                                <div id="question4" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>OnDeck deducts a fixed daily or weekly payment directly from your business bank account each business day, which helps to ensure minimal impact to your cash flow. These small, regular payments are proven to prevent the snowball effect often caused by missing larger monthly payments. Our online loan management portal allows you to monitor this activity 24/7 365 days a year.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question5">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>What industries does OnDeck work with?</h3></a>
                                    </div>

                                </div>
                                <div id="question5" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>OnDeck works with over 700 different kinds of businesses. We make loans to restaurants, auto repair shops, retailers, doctors, beauty salons, HVAC contractors and many other service providers.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question6">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>Is my small business eligible for a loan with OnDeck?</h3></a>
                                    </div>

                                </div>
                                <div id="question6" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>To apply, your business should have the following characteristics:</p>
                                        <ul class="business_points">
                                            <li>1+ year in business</li>
                                            <li>$100,000+ in annual revenue</li>
                                            <li>*other restrictions may apply</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question7">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>I don't have great personal credit. Can I still get a loan with OnDeck?</h3></a>
                                    </div>

                                </div>
                                <div id="question7" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>Yes. At OnDeck we know that your personal credit score doesn’t reflect the true health of your business. That’s why our system values things like cash flow over credit scores when we analyze your business.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question8">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>I've been denied by my bank before. Can I still get a loan with OnDeck?</h3></a>
                                    </div>

                                </div>
                                <div id="question8" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>Yes. Our system judges small businesses differently than the banks, with a focus on cash flow instead of personal credit scores or collateral. This means that we can lend to many businesses who may have been denied by their bank.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question9">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>What information do you need from me to apply?</h3></a>
                                    </div>

                                </div>
                                <div id="question9" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>Depending on your loan amount, we may ask for:</p>
                                        <ul class="business_points">
                                            <li>Business Tax ID</li>
                                            <li>Credit card statements for the previous 3 months</li>
                                            <li>Bank statements for the previous 1 to 3 months</li>
                                            <li>Social Security number of business owner(s)</li>
                                            <li>Drivers license number and state of issue</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question10">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>How quickly does OnDeck submit funding decisions?</h3></a>
                                    </div>

                                </div>
                                <div id="question10" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>OnDeck can provide decisions in minutes.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question11">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>How fast can I receive funds?</h3></a>
                                    </div>

                                </div>
                                <div id="question11" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>OnDeck can provide funding in as fast as 1 business day.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question12">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>Do you file a UCC?</h3></a>
                                    </div>

                                </div>
                                <div id="question12" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>We do. However, you may keep your OnDeck account when borrowing from a separate financial institution.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question13">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>Do you report to credit bureaus?</h3></a>
                                    </div>

                                </div>
                                <div id="question13" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>We do. However, you may keep your OnDeck account when borrowing from a separate financial institution.Absolutely! We report your payments on a regular basis to the major credit bureaus, so you can build business credit. Please note it may take several months for your line to show up in your credit report, depending on individual credit bureau reporting practices.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question14">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>How will this loan affect my credit?</h3></a>
                                    </div>

                                </div>
                                <div id="question14" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>Other lenders look at your credit limit, borrowing history, and payment track record to make lending decisions; your good payment record with OnDeck will be available for them to view.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question15">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>I have another question. Who should I call?</h3></a>
                                    </div>

                                </div>
                                <div id="question15" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>Please don’t hesitate to call your account manager directly. His or her direct contact info is displayed on your account home page.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question16">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>It's been more than 48 hours and I haven't received my funds, what do I do?</h3></a>
                                    </div>

                                </div>
                                <div id="question16" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>Any concerns regarding not receiving funds or a payment looking incorrect can be taken care of by our Customer Service team, which can be reached at (888) 828-5717</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question17">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>It's been more than 48 hours and I haven't received my funds, what do I do?What is the application process?</h3></a>
                                    </div>

                                </div>
                                <div id="question17" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>Applying for an OnDeck loan is simple and typically takes about 10 minutes. You can apply online in minutes or over the phone by calling (888) 828-5717. We can provide funding decisions within minutes.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question18">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>How do I go about changing my bank account?</h3></a>
                                    </div>

                                </div>
                                <div id="question18" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>Any concerns regarding not receiving funds or a payment looking incorrect can be taken care of by our Customer Service team, which can be reached at (888) 269-4246 + option 3.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="content_header">
                                <h4 class=" mail_sec"><strong>About Your Term Loan</strong></h4>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question19">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>Can I apply for a second loan if I need additional financing?</h3></a>
                                    </div>

                                </div>
                                <div id="question19" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>Yes, we offer customers the option to apply for additional financing. When you are eligible depends on your term length. A Renewal Specialist will reach out to you directly once you are eligible to renew.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question20">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>When do I start making payments</h3></a>
                                    </div>

                                </div>
                                <div id="question20" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>Payments begin as soon as you receive your loan. We schedule daily payments every weekday at 11 am; expect to see them debited from your account on the next business day.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default adjust_border ">
                                <div class="panel-heading accordion-toggle question-toggle collapsed accordian_colr" data-toggle="collapse" data-parent="#faqAccordion" data-target="#question21">
                                    <div class="panel-title">
                                        <a href="javascript:void()" class="ing"><h3>What happens if I miss a payment?</h3></a>
                                    </div>

                                </div>
                                <div id="question21" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body accordian_font">
                                        <p>If you think you're going to miss a payment, please don't hesitate to contact us. If you do miss a payment, we'll notify you via email, and schedule an immediate make-up payment, which you'll see 3-4 days after the original payment.
                                        <span class="m-t-20">We'll also reach out - usually after your second missed payment - to see if we can help.</span>
                                        </p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!--/panel-group-->
            </div>
        </div>
        </div>
        @stop