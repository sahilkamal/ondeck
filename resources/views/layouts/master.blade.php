<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>CrowdBanking</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" href="css/font-awesome.min.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="css/style.css">
    </head>

    <body>
        <header>
           <!--start pre Header-->
            <div class="pre_header">
                <div class="container">
                    <ul class="pre_header_list pull-right">
                        <li><a href="javascript:void(0);" id="chat_live" onclick="showChat()"><i class="fa fa-comment" aria-hidden="true"></i> chat live</a></li>
                        <li><strong>Call Us:</strong>1.888 726.2198</li>
                    </ul>
                </div>
            </div>
            <!--End pre Header-->

            <!--Star Main Header -->
            <div class="bg-clr">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 ">
                            <div class="col-md-2 col-sm-12">
                                <div class="logo_main" style="width:100%; display:block;">
                                    <div class="logo">
                                        <a href="/dashboard">
                                            <img src="images/other_logo.jpg" alt="No-Img" class="img-responsive">
                                        </a>           
                                    </div>
                            
                                <div class="mobile_btn">
                                    <a href="#" class="mobile_menu pull-right colr_text"></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8 col-sm-12">
                            <nav class="nav_main">
                                <ul class="nav-bar">
                                    <li {{{ (Request::is('dashboard') ? 'class=narrow_icon' : '') }}}><a href="dashboard">Dashboard</a></li>
                                     <li {{{ (Request::is('myloans') ? 'class=narrow_icon' : '') }}}><a href="myloans">Create Loan</a></li>
                                    <li {{{ (Request::is('withdraw') ? 'class=narrow_icon' : '') }}}><a href="withdraw"><span class="">Withdraw</span></a></li>
                                    <li {{{ (Request::is('activity') ? 'class=narrow_icon' : '') }}}><a href="activity"><span class="">Activity</span></a></li>
                                    <li {{{ (Request::is('documents') ? 'class=narrow_icon' : '') }}}><a href="documents"><span class="">Documents</span></a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-md-2 col-sm-12">
                            <ul class="main_dropdown pull-right">
                                <li class="dropdown pull-right">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">@if (Auth::guest())  @else{{ Auth::user()->name }} @endif  <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="update-profile"><i class="fa fa-user" aria-hidden="true"></i>profile</a></li>
                                        <li><a href="administration"><i class="fa fa-user" aria-hidden="true"></i>Administration</a></li>
                                        <li><a href="help"><i class="fa fa-info-circle" aria-hidden="true"></i>Help</a></li>
                                        <li>
                                        <a href="{{ route('logout')}}" onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i>Sign Out</a><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}</form></li>
                                    </ul>
                                </li>
                                <li>International Manage...</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <!-- End Main Header-->
        </header>
        <div class="main_content">
            @yield('content')
            <!-- chat Box-->
                <div class="row chat-window col-xs-5 col-md-3" id="chat_window" style="">
                    <div class="col-xs-12 col-md-12">
                        <div class="panel panel-default">
                            <div class="panel-heading top-bar">
                                <div class="col-md-8 col-xs-8">
                                    <h3 class="panel-title white_clr">Chat with us </h3>
                                </div>
                                <div class="col-md-4 col-xs-4" style="text-align: right;">
                                    <a href="#"><span id="minim_chat_window" class="icon_minim white_clr"><i class="minimize_icon fa fa-minus" aria-hidden="true"></i></span></a>
                                    <a href="#"><span class="icon_close white_clr" data-id="chat_window_1"><i class="fa fa-times" aria-hidden="true"></i></span></a>
                                </div>
                            </div>
                            <div class="panel-body msg_container_base">
                                <form>
                                    <div class="inputs">
                                        <div class="input-group " class="">
                                            <span>Your Name*</span>
                                            <input type="text" required value="" placeholder="Enter text here" class="form-control input_height">
                                        </div>
                                    </div>
                                    <div class="inputs">
                                        <div class="input-group " class="">
                                            <span>Phone Number*</span>
                                            <input type="tel" required value="" placeholder="Enter text here" class="form-control input_height">
                                        </div>
                                    </div>
                                    <div class="inputs">
                                        <div class="input-group " class="">
                                            <span>Email Address</span>
                                            <input type="text" required value="" placeholder="Enter text here" class="form-control input_height">
                                        </div>
                                    </div>
                                    <div class="inputs">
                                        <div class="input-group " class="">
                                            <span>Business Name</span>
                                            <input type="text" required value="" placeholder="Enter text here" class="form-control input_height">
                                        </div>
                                    </div>
                                    <div class="best_reason">
                                        <p>Which option best describes your reason for contacting OnDeck?*</p>
                                    </div>
                                    <div class="options">
                                        <select>
                                            <option class="-1">Choose item from the list </option>
                                            <option value="0">
                                                I am a business owner and want information about OnDeck loans.</option>
                                            <option value="1">
                                                I’ve already submitted a loan application, and want to check on the status.</option>
                                            <option value="2">
                                                I have not yet started a business and want information about OnDeck loans.</option>
                                            <option value="3">
                                                I am a current OnDeck customer and have questions about my account.</option>
                                            <option value="4">
                                                I want to become an OnDeck partner or already am an OnDeck partner.</option>
                                            <option value="5">
                                                I have questions about payments on my OnDeck loan.
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form_btns">
                                        <a href="#" class="deny_btn">Cancel</a>
                                        <input type="button" value="submit">
                                        <div class="bottom_space"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--End Chat Box-->
        </div>

        <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="main_footer">
                                <ul class="footer_content">
                                    <li><img src="images/logo.svg" alt="No-img" class="img-responsive"></li>
                                    <li><i class="fa fa-phone" aria-hidden="true"></i> <p class="fone_number">1.888.726.2198</p></li>
                                    <li><i class="fa fa-envelope-o" aria-hidden="true"></i> <p class="fone_number"><a href="#">info@ondeck.com</a></p></li>
                                    <li><i class="fa fa-comment" aria-hidden="true"></i> <p class="fone_number"><a href="#">Chat Live</a></p></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="body_color">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="registeration_sec">
                                    <ul class="footer_content">
                                        <li>Loans subject to lender approval. </li>
                                        <li>OnDeck® is a registered trademark.</li>
                                        <li>All rights reserved. </li>
                                        <li><a href="#">Privacy Policy</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </footer>
        <!-- End crow_1_2-->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/site/mask.js"></script>
<script src="js/moment-with-locales.min.js"></script>
<script src="js/bootstrap-datetimepicker.min.js"></script>
<script src="js/progressbar.js"></script>
<script src="js/custom.js"></script>
<script src="js/site/dashboard.js"></script>

<!-- calander script-->
            <script>
                $(function () {
                    var bindDatePicker = function() {
                        $(".date").datetimepicker({
                            format:'YYYY-MM-DD',
                            icons: {
                                time: "fa fa-clock-o",
                                date: "fa fa-calendar",
                                up: "fa fa-arrow-up",
                                down: "fa fa-arrow-down"
                            }
                        }).find('input:first').on("blur",function () {
                            // check if the date is correct. We can accept dd-mm-yyyy and yyyy-mm-dd.
                            // update the format if it's yyyy-mm-dd
                            var date = parseDate($(this).val());

                            if (! isValidDate(date)) {
                                //create date based on momentjs (we have that)
                                date = moment().format('YYYY-MM-DD');
                            }

                            $(this).val(date);
                        });
                    }

                    var isValidDate = function(value, format) {
                        format = format || false;
                        // lets parse the date to the best of our knowledge
                        if (format) {
                            value = parseDate(value);
                        }

                        var timestamp = Date.parse(value);

                        return isNaN(timestamp) == false;
                    }

                    var parseDate = function(value) {
                        var m = value.match(/^(\d{1,2})(\/|-)?(\d{1,2})(\/|-)?(\d{4})$/);
                        if (m)
                            value = m[5] + '-' + ("00" + m[3]).slice(-2) + '-' + ("00" + m[1]).slice(-2);

                        return value;
                    }

                    bindDatePicker();
                });

                $('.mobile_menu').on('click', function(){
                $('.nav_main').toggle();

            });
        $(function(){
            $.ajaxSetup({
             headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    });
    });
            </script>
</body>
</html>