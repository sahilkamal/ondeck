<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('amount');
            $table->string('status');
            $table->string('type');
            $table->string('principal');
            $table->string('interest');
            $table->string('loan_period');
            $table->integer('loans_user_id_foreign')->unsigned();
            $table->foreign('loans_user_id_foreign')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::dropIfExists('loans');
    }
}
